import Vue from 'vue'
import ElementUI from 'element-ui'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets
import '@base/styles/index.scss' // global css

import App from '@base/App'
import router from '@base/router'
import store from '@base/store'

import '@base/icons' // icon
import '@base/permission' // permission control
import '@logic/main' // 扩展业务自定义方法等
import Page from '@base/components/Page' // 扩展业务自定义方法等

if (process.env.NODE_ENV === 'development') {
  Vue.config.devtools = true
}

Vue.component('Page', Page)
Vue.prototype.$log = window.console.log
Vue.use(ElementUI, { size: 'small' })
Vue.config.productionTip = false

const vm = new Vue({
  el: '#app',
  router,
  store,
  render: (h) => h(App)
})

Vue.use({ vm })
