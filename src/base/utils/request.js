import axios from 'axios'
import { Message, Loading } from 'element-ui'
import Qs from 'qs'
import store from '../store'
import { getToken } from '@base/utils/auth'
import { debounce } from '@base/utils'

let loadingInstance = null
let loadingCount = 0

// 创建axios实例
const service = axios.create({
  timeout: 30000 // 请求超时时间
})

service.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'

// request拦截器
service.interceptors.request.use(
  (config) => {
    if (store.getters.token) {
      config.headers.Authorization = getToken() // 让每个请求携带自定义token 请根据实际情况自行修改
    }
    return config
  },
  (error) => {
    // Do something with request error
    debouncePopError('网络开小差了 请稍等一会儿')
    closeLoading()
    Promise.reject(error)
  }
)

// response 拦截器
service.interceptors.response.use((response) => {
  return response.data
})

const request = async (method, url, params, config = { isFilter: true, isLoading: true }) => {
  const { isFilter = true, isLoading = true } = config

  if (isLoading !== false) {
    startLoading()
  }
  try {
    let res = null
    if (method === 'get') {
      let query = ''
      if (typeof params !== 'string') {
        query = Qs.stringify(params)
      }
      if (query) {
        url += (url.indexOf('?') > -1 ? '&' : '?') + query
      }
      res = await service.get(url, { headers: { isLoading } })
    } else if (method === 'post') {
      res = await service.post(url, params, { headers: { isLoading } })
    } else if (method === 'delete') {
      res = await service.delete(url, { headers: { isLoading } })
    } else if (method === 'put') {
      res = await service.put(url, params, { headers: { isLoading } })
    }

    if (isLoading !== false) {
      closeLoading()
    }
    const { message, result, status, code, data } = res
    const tokenCodeErrList = [
      52000 // 未登录、登录超时
    ]

    if (status === 'OK') {
      if (isFilter) {
        // 因为框架问题需要保证兼容性
        return message || result
      } else {
        return res
      }
    } else if (code === 200) {
      if (isFilter) {
        return data
      } else {
        return res
      }
    } else {
      // 登录状态有问题，重置后跳转到登录页面
      if (tokenCodeErrList.indexOf(+message) >= 0) {
        store.dispatch('FedLogOut').then(() => {
          debouncePopError('登录超时，请重新登录')
        })
      } else {
        debouncePopError(message)
      }
      // reject该错误，并不会进入catch
      return Promise.reject(res)
    }
  } catch (e) {
    debouncePopError('网络开小差了 请稍等一会儿')
    if (isLoading !== false) {
      closeLoading()
    }
    throw e
  }
}

const debouncePopError = debounce(function (message = '网络开小差了 请稍等一会儿！') {
  Message.error({ message: message, showClose: true })
}, 500)

function startLoading () {
  if (loadingCount === 0 && !loadingInstance) {
    loadingInstance = Loading.service({ fullscreen: true })
  }
  loadingCount++
}

function closeLoading () {
  loadingCount--
  loadingCount = Math.max(loadingCount, 0)
  if (loadingCount === 0) {
    toCloseLoading()
  }
}

const toCloseLoading = debounce(() => {
  loadingInstance && loadingInstance.close()
  loadingInstance = null
}, 300)

/***
 * 正常请求方法，支持过滤返回结果
 * @param url
 * @param params
 * @param config = {
 *   isFilter = true, 是否直接滤出结果，不返回code和msg
 *   isLoading = true，是否展示loading动画
 *  }
 * @returns {Promise<*|null|undefined>}
 */
const httpGet = (url, params = {}, config) => request('get', url, params, config)

const httpPost = (url, params = {}, config) => request('post', url, params, config)

const httpDelete = (url, params = {}, config) => request('delete', url, params, config)

const httpPut = (url, params = {}, config) => request('put', url, params, config)

/***
 * 兼容老接口对应的请求方法，不支持过滤返回结果
 * @param url
 * @param params
 * @param config = {
 *   isFilter = false, 默认false，是否直接滤出结果，不返回code和msg
 *   isLoading = true，是否展示loading动画
 *  }
 * @returns {Promise<*|null|undefined>}
 */
const baseHttpGet = (url, params = {}, config) => request('get', url, params, { isFilter: false, ...config })

const baseHttpPost = (url, params = {}, config) => request('post', url, params, { isFilter: false, ...config })

const baseHttpDelete = (url, params = {}, config) => request('delete', url, params, { isFilter: false, ...config })

const baseHttpPut = (url, params = {}, config) => request('put', url, params, { isFilter: false, ...config })

// 上传
const httpUpload = async (url, file) => {
  let res = null
  const uploadAxios = axios.create()
  uploadAxios.defaults.timeout = 60000 * 30
  const config = {
    headers: {
      'Content-Type': 'multipart/form-data'
    },
    responseType: 'arraybuffer'
  }
  res = await uploadAxios.post(url, file, config)

  if (res.headers['content-type'].indexOf('application/vnd.ms-excel') !== -1) {
    const blob = new Blob([res.data])
    const alink = document.createElement('a')
    const remoteName = decodeURI(res.headers['content-disposition'].split('filename=')[1])
    if (remoteName) {
      alink.download = remoteName
    } else {
      alink.download = '导入失败信息.xlsx'
    }
    alink.style.display = 'none'
    alink.href = URL.createObjectURL(blob) // 这里是将文件流转化为一个文件地址
    document.body.appendChild(alink)
    alink.click()
    URL.revokeObjectURL(alink.href) // 释放URL 对象
    document.body.removeChild(alink)
    return { status: 'FAIL', message: '导入失败' }
  } else {
    const enc = new TextDecoder('utf-8')
    return JSON.parse(enc.decode(new Uint8Array(res.data))) // 转化成json对象
  }
}

// 导出
const httpExport = async (method, url, params, fileName) => {
  try {
    let res = null
    const exportAxios = axios.create()
    exportAxios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
    exportAxios.defaults.responseType = 'arraybuffer'
    if (method === 'get') {
      let query = ''
      if (typeof params !== 'string') {
        query = Qs.stringify(params)
      }
      if (query) {
        url += (url.indexOf('?') > -1 ? '&' : '?') + query
      }
      res = await exportAxios.get(url)
    } else if (method === 'post') {
      res = await exportAxios.post(url, params)
    } else if (method === 'delete') {
      res = await exportAxios.delete(url)
    } else if (method === 'put') {
      res = await exportAxios.put(url, params)
    }
    const remoteName = decodeURI(res.headers['content-disposition'].split('filename=')[1])
    if (res.headers['content-type'].indexOf('application/vnd.ms-excel') !== -1) {
      const blob = new Blob([res.data])
      const alink = document.createElement('a')
      if (fileName) {
        alink.download = fileName + '.csv'
      } else if (remoteName) {
        alink.download = remoteName
      } else {
        alink.download = Date.now() + '.csv'
      }
      alink.style.display = 'none'
      alink.href = URL.createObjectURL(blob) // 这里是将文件流转化为一个文件地址
      document.body.appendChild(alink)
      alink.click()
      URL.revokeObjectURL(alink.href) // 释放URL 对象
      document.body.removeChild(alink)
      return { status: 'FAIL', message: '导入失败' }
    } else {
      const enc = new TextDecoder('utf-8')
      const result = JSON.parse(enc.decode(new Uint8Array(res.data))) // 转化成json对象
      return result
    }
  } catch (e) {
    debouncePopError('网络开小差了 请稍等一会儿')
    throw e
  }
}

// 下载模板
const httpDownload = async (url, fileName) => {
  axios({
    method: 'get',
    url,
    responseType: 'blob' // 表明服务器返回的数据类型
  }).then(function (res) {
    const blob = new Blob([res.data], { type: 'application/vnd.ms-excel' })
    fileName = fileName + '.xlsx'
    if ('download' in document.createElement('a')) {
      // 非IE下载
      const aEle = document.createElement('a')
      aEle.download = fileName
      aEle.style.display = 'none'
      aEle.href = URL.createObjectURL(blob)
      document.body.appendChild(aEle)
      aEle.click()
      URL.revokeObjectURL(aEle.href) // 释放URL 对象
      document.body.removeChild(aEle)
    } else {
      // IE10+下载
      navigator.msSaveBlob(blob, fileName)
    }
  })
}

export {
  request,
  httpGet,
  httpPost,
  httpDelete,
  httpPut,
  httpUpload,
  httpExport,
  httpDownload,
  baseHttpGet,
  baseHttpPost,
  baseHttpDelete,
  baseHttpPut
}
