export default class Cache {
  _clone (object) {
    return !object ? object : JSON.parse(JSON.stringify(object))
  }
}
