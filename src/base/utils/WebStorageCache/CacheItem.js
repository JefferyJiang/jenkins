export default class CacheItem {
  /**
   * Creates an instance of CacheItem.
   * @param {number} timeout  过期秒数
   * @param {*} value 缓存值
   * @param {number} createtime 创建时间
   *
   * @memberOf CacheItem
   */
  constructor (timeout, _value, createtime, key) {
    this.timeout = timeout
    this._value = _value
    this.createtime = createtime
    this.key = key
  }

  /** 是否过期 */
  get isExpired () {
    if (!this.timeout) {
      return false
    }
    const targetTime = this.createtime + this.timeout * 1000
    const balance = targetTime - new Date().getTime()
    return balance < 0
  }

  /** 获取值 */
  get value () {
    return this._value
  }
}
