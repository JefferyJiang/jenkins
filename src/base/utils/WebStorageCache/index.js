import Cache from './Cache'
import CacheItem from './CacheItem'
export default class WebStorageCache extends Cache {
  constructor (cacheId) {
    super()
    this.KEY_CACHE = cacheId || this.randomString(32)
  }

  set (key, value, timeout) {
    try {
      this._set(key, value, timeout)
    } catch (e) {
      this.clear()
      try {
        this._set(key, value, timeout)
        return true
      } catch (e1) {
        return false
      }
    }
    return true
  }

  _set (key, value, timeout) {
    const $cache = this.$cache
    $cache[key] = new CacheItem(timeout, value, new Date().getTime(), key)
    localStorage.setItem(this.KEY_CACHE, JSON.stringify($cache))
  }

  get (key) {
    this.removeExpired()
    const $cache = this.$cache
    return $cache[key] ? $cache[key]._value : undefined
  }

  hasKey (key) {
    this.removeExpired()
    return !!this.$cache[key]
  }

  remove (key) {
    this.removeExpired()
    if (this.hasKey(key)) {
      const $cache = this.$cache[key]
      const r = $cache[key]
      $cache[key] === undefined && delete $cache[key]
      localStorage.setItem(this.KEY_CACHE, JSON.stringify($cache))
      return r
    }
    return undefined
  }

  clear () {
    localStorage.removeItem(this.KEY_CACHE)
  }

  removeExpired () {
    const $cache = this.$cache
    const r = {}
    Object.keys($cache).forEach((key) => {
      const v = $cache[key]
      if (v && !new CacheItem(v.timeout, v._value, v.createtime, v.key).isExpired) {
        r[key] = $cache[key]
        $cache[key] = undefined
      }
    })
    localStorage.setItem(this.KEY_CACHE, JSON.stringify(r))
  }

  get $cache () {
    return JSON.parse(localStorage.getItem(this.KEY_CACHE) || '{}')
  }

  randomString (length) {
    var str = ''
    for (; str.length < length; str += Math.random().toString(36).substr(2));
    return '$$' + str.substr(0, length) + '$$'
  }
}
// # sourceMappingURL=webstorage.cache.js.map
