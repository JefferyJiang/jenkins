import WebStorageCache from '@base/utils/WebStorageCache'

const PUBLIC_KEY = '$$user-sessionid$$'
const BUSINESS_KEY = 'business-key'
const USER_KEY = 'user-authentication-key'

const sessionCache = new WebStorageCache(PUBLIC_KEY)

export function setBusinessKey (value) {
  sessionCache.set(BUSINESS_KEY, value)
}

export function getBusinessKey () {
  return sessionCache.get(BUSINESS_KEY)
}

export function setUser (value) {
  sessionCache.set(USER_KEY, value)
}

export function getUser () {
  return sessionCache.get(USER_KEY)
}

export function getBusinessKeyByUrl () {
  ;/^\/([-A-Za-z0-9+&@#%=~_|]*)\//.exec(location.pathname)
  return RegExp.$1 || ''
}
