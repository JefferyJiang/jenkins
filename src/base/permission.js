import router from './router'
import store from './store'
import NProgress from 'nprogress' // Progress 进度条
import 'nprogress/nprogress.css' // Progress 进度条样式
import { getUser } from '@base/utils/logicUtils' // 验权

const whiteList = ['/login'] // 不重定向白名单
router.beforeEach(async (to, from, next) => {
  NProgress.start()
  if (getUser()) {
    if (to.path === '/login') {
      return next({ path: '/home' })
    } else {
      const { businessList, info } = store.state.user
      const { menu } = store.state.permission

      if (!info) {
        store.dispatch('GetUserInfo')
      }

      if (!businessList || !menu) {
        await store.dispatch('GetBusinessList')
        store.dispatch('GenerateRoutes').then(() => {
          // 生成可访问的路由表
          router.addRoutes(store.state.permission.accessRoute) // 动态添加可访问路由表
          return next({ ...to, replace: true })
        })
      } else {
        return next()
      }
    }
  } else {
    if (whiteList.indexOf(to.path) !== -1) {
      return next()
    } else {
      return next('/login')
    }
  }
})

router.afterEach(() => {
  NProgress.done() // 结束Progress
})
