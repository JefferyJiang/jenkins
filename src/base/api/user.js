import { httpGet, httpPost } from '@base/utils/request'
import { PERMISSION_PROXY, FRAMEWORK_PROXY } from '@base/const/proxy'

// 根据业务系统获取菜单
export function getMenu (projectName) {
  if (!projectName) {
    return Promise.resolve([])
  }
  return httpGet(`${PERMISSION_PROXY}/menu/tree/byBusinessCode/${projectName}`)
}

// 获取业务系统列表
export function getBusinessList (businessCode) {
  return httpGet(`${PERMISSION_PROXY}/menu/getBusinessList`)
}

// 获取按钮权限
export function getButtonAuth (userCode) {
  return httpGet(`${PERMISSION_PROXY}/userButton/alloted/btn/{userCode}`)
}

// 获取用户信息
export function getUserInfo (params) {
  return httpGet(`${PERMISSION_PROXY}/users/loginUser`)
}

// 获取用户配置菜单列表
export function getConfig (userCode) {
  return httpGet(`${FRAMEWORK_PROXY}/businessConfig/getConfig`)
}

// 保存用户配置菜单列表
export function saveConfig (params) {
  return httpPost(`${FRAMEWORK_PROXY}/businessConfig/saveConfig`, params)
}
