import Vue from 'vue'
import Vuex from 'vuex'
import app from './modules/app'
import user from './modules/user'
import permission from './modules/permission'
import getters from './getters'
import { store as logicStore, getters as logicGetters } from '@logic/store'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    user,
    permission,
    ...logicStore
  },
  getters: {
    ...getters,
    ...logicGetters
  }
})

export default store
