import { login, logout } from '@base/api/login'
import { getBusinessList, getUserInfo } from '@base/api/user'
import { removeAll, setToken } from '@base/utils/auth'
import { setBusinessKey, setUser, getBusinessKeyByUrl } from '@base/utils/logicUtils'
import { local } from '@base/utils/storage'
import router from '@base/router'

const user = {
  state: getInitialState(),
  mutations: {
    SET_INFO: (state, info) => {
      state.info = info
      setToken(info.id)
    },
    SET_BUSINESS: (state, businessList) => {
      state.businessList = businessList
    },
    SET_CURRENT_BUSINESS: (state, currentBusinessCode = getBusinessKeyByUrl()) => {
      if (Array.isArray(state.businessList)) {
        state.currentBusiness = state.businessList.find((business) => {
          return business.code === currentBusinessCode
        })
        setBusinessKey(currentBusinessCode)
      }
    },
    RESET_USER (state) {
      state = getInitialState()
      removeAll()
      router.push(`/login?redirect=${router.currentRoute.fullPath}`)
    }
  },

  actions: {
    // 登录
    Login ({ commit }, userInfo) {
      return new Promise((resolve, reject) => {
        login(userInfo)
          .then((info = {}) => {
            const { code } = info
            setUser({
              ...info,
              userName: code
            })
            commit('SET_INFO', info)
            resolve(info)
          })
          .catch((error) => {
            reject(error)
          })
      })
    },

    // 获取业务系统列表
    GetBusinessList ({ commit, state }) {
      return new Promise((resolve, reject) => {
        getBusinessList().then((businessList) => {
          const businessKey = getBusinessKeyByUrl()
          commit('SET_BUSINESS', businessList)
          // 取url对应的businessKey
          if (businessKey) {
            commit('SET_CURRENT_BUSINESS', businessKey)
          }
          resolve(businessList)
        })
      })
    },

    // 获取用户信息
    GetUserInfo ({ commit, state }) {
      return new Promise((resolve, reject) => {
        getUserInfo().then((info = {}) => {
          commit('SET_INFO', info)
          resolve(info)
        })
      })
    },

    // 登出
    LogOut ({ commit, state }) {
      return new Promise((resolve, reject) => {
        logout()
          .then(() => {
            commit('RESET_USER')
            resolve()
          })
          .catch((error) => {
            reject(error)
          })
      })
    },

    // 前端 登出
    FedLogOut ({ commit }) {
      return new Promise((resolve) => {
        commit('RESET_USER')
        removeAll()
        local.clear()
        resolve()
      })
    }
  }
}

// 获取初始state
function getInitialState () {
  return {
    info: null,
    businessList: null,
    currentBusiness: {}
  }
}

export default user
