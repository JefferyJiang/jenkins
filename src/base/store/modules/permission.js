import { localAsyncRoutes } from '@base/router'
import { getMenu } from '@base/api/user'
import { getBusinessKeyByUrl } from '@base/utils/logicUtils'

const state = {
  // 动态权限路由map
  accessRoutesMap: null,
  // 根据接口返回的菜单权限计算出对应的路由权限，通过addRoutes添加
  accessRoute: null,
  menu: null
}

const mutations = {
  SET_ROUTE: (state, { accessRoutesMap, accessRoute }) => {
    state.accessRoutesMap = accessRoutesMap
    state.accessRoute = accessRoute
  },
  SET_MENU: (state, menu) => {
    if (Array.isArray(menu) && menu.length > 0 && Array.isArray(menu[0].items)) {
      menu[0].items.unshift({
        code: 'Home',
        isMenu: 1,
        label: '首页',
        routerLink: '/home',
        url: '/home',
        icon: 'el-icon-house'
      })
    }
    state.menu = menu
  }
}

const actions = {
  // 生成本地路由及接口路由字典
  GenerateRoutes ({ commit }) {
    const code = getBusinessKeyByUrl()
    return getMenu(code).then((menu = {}) => {
      commit('SET_MENU', menu)
      const accessRoutesMap = getAccessRouteMap(menu)
      const accessRoute = getAccessRoute(accessRoutesMap)
      commit('SET_ROUTE', { accessRoutesMap, accessRoute })
      return menu
    })
  }
}

function getAccessRouteMap (menu = [], accessRouteMap = {}) {
  if (Array.isArray(menu)) {
    menu.forEach((route) => {
      const { items = [], code } = route
      if (Array.isArray(items) && items.length > 0) {
        getAccessRouteMap(items, accessRouteMap)
      }
      if (code) {
        accessRouteMap[code] = route
      }
    })
  }
  return accessRouteMap
}

function getAccessRoute (accessRouteMap, asyncRoutes = localAsyncRoutes) {
  return asyncRoutes.filter((route) => {
    const permission = getPermission(accessRouteMap, route)
    /***
     * name配置规范
     * name统一设置会接口的code字段
     * 1: 如果接口存在目录，则对应的目录下所有目录或链接都会具有权限
     * 2: 如果接口不存在目录，本地有目录则该目录不需要配置name默认有权限，递归遍历其内部的children中的权限，
     *    如果主链接有权限，但从主链接中衍生出来的子链接（比如新增，修改，查看，编辑页面）权限并不在接口的返回中，
     *    把主链接看作是目录，将子链接name设置为 主链接code|子链接name随意写
     */
    if (permission.hasAuth) {
      if (route.children && route.children.length > 0 && !permission.isEnd) {
        route.children = getAccessRoute(accessRouteMap, route.children)
      }
      return true
    }
    return false
  })
}

function getPermission (accessRouteMap, route) {
  let name = route.name
  const permission = {
    hasAuth: false,
    isEnd: true
  }
  if (name) {
    // 返回了目录权限，该目录下的子链接，如果有目录权限代表该链接也有权限访问
    const isChildLinkWithoutAuth = name.split('|').length > 1
    if (isChildLinkWithoutAuth) {
      name = name.split('|')[0]
    }

    if (accessRouteMap[name] && isChildLinkWithoutAuth) {
      permission.hasAuth = true
      permission.isEnd = true
    } else if (accessRouteMap[name]) {
      const accessRoute = accessRouteMap[name]
      permission.hasAuth = true
      if (accessRoute.icon && route.meta) {
        route.meta.icon = accessRouteMap[name].icon
      }
      if (accessRoute.label) {
        route.meta = {
          ...route.meta,
          title: accessRouteMap[name].label
        }
      }

      const children = accessRouteMap[name].items
      if (
        Array.isArray(route.children) &&
        route.children.length > 0 &&
        Array.isArray(children) &&
        children.length > 0
      ) {
        children[0].url && (route.redirect = children[0].url)
        permission.isEnd = false
      }
    }
  } else {
    permission.hasAuth = true
    if (Array.isArray(route.children) && route.children.length > 0) {
      permission.isEnd = false
    }
  }

  return permission
}

export default {
  state,
  mutations,
  actions
}
