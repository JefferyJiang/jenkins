const getters = {
  sidebar: (state) => state.app.sidebar,
  device: (state) => state.app.device,
  userInfo: (state) => state.user.info,
  businessList: (state) => state.user.businessList,
  currentBusiness: (state) => state.user.currentBusiness,
  menu: (state) => state.permission.menu
}
export default getters
