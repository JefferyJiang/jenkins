import Layout from '@base/views/layout/Layout.vue'

export default [
  {
    path: '/order',
    component: Layout,
    name: 'order',
    children: [
      {
        path: 'listTest',
        name: 'orderListTest',
        component: () => import('@logic/views/order/list/index.vue'),
        meta: {
          title: '列表测试'
        }
      },
      {
        path: 'report',
        name: 'orderReport',
        component: () => import('@logic/views/order/list/index.vue'),
        meta: {
          title: '订单报表'
        }
      },
      {
        path: 'detail',
        name: 'orderListTest|detail',
        component: () => import('@logic/views/order/detail/index.vue'),
        meta: {
          title: '详情页'
        }
      }
    ]
  },
  {
    path: '/person',
    component: Layout,
    children: [
      {
        path: 'info',
        name: 'personInfo',
        component: () => import('@logic/views/person/info/index.vue'),
        meta: {
          title: '人员信息'
        }
      }
    ]
  },
  {
    path: '*',
    redirect: '/404'
  }
]
