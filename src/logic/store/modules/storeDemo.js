const storeDemo = {
  state: {
    projectName: 'vue-backend-starter'
  },
  mutations: {
    CHANGE_PROJECT_NAME: (state) => {
      state.projectName = 'vue-backend-starter-new'
    }
  },
  actions: {}
}

export default storeDemo
