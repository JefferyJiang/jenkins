import storeDemo from './modules/storeDemo'

const store = {
  storeDemo
}

const getters = {
  storeDemo: (state) => state.storeDemo
}

export { store, getters }
