import { httpGet } from '@utils/request.js'
import api from '@logic/config/api'

export default class List {
  // 查找品牌会员权益列表
  static async selectByPageByFilter (params) {
    return await httpGet(api.order.list.selectByPageByFilter, params)
  }
}
