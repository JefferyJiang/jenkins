export default {
  name: 'equity-manage-detail',
  computed: {
    rowData () {
      return this.$route.params.rowData || {}
    },
    paging () {
      return this.$route.params.paging || {}
    }
  },
  methods: {
    clickToBack () {
      this.$router.push({
        name: 'list',
        params: {
          paging: {
            page: this.paging.page,
            rows: this.paging.rows
          }
        }
      })
    }
  }
}
