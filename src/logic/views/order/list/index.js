import listService from '@logic/services/order/list'

export default {
  name: 'equity-manage',
  data () {
    return {
      form: {
        page: null,
        rows: null,
        sidx: 'id',
        sord: 'desc'
      },
      tableData: null,
      totalRecord: null,
      // 常量推荐使用Object.freeze
      pageList: Object.freeze([
        {
          label: '第一页',
          value: 1
        },
        {
          label: '第二页',
          value: 2
        },
        {
          label: '第三页',
          value: 3
        }
      ]),
      // 常量推荐使用Object.freeze
      rowList: Object.freeze([
        {
          label: '2条',
          value: 2
        },
        {
          label: '4条',
          value: 4
        },
        {
          label: '6条',
          value: 6
        },
        {
          label: '8条',
          value: 8
        }
      ])
    }
  },
  computed: {
    paging () {
      return this.$route.params.paging || {}
    }
  },
  async created () {
    this.form.page = this.paging.page || 1
    this.form.rows = this.paging.rows || 8
    // 路由name配置
    // 方法名称建议和接口名称统一
    await this.selectByPageByFilter()
  },
  methods: {
    resetForm (formName) {
      this.$refs[formName].resetFields()
    },
    clickToCheck (rowData) {
      this.$router.push({
        name: 'list|detail',
        params: {
          rowData,
          paging: {
            page: this.form.page,
            rows: this.form.rows
          }
        }
      })
    },
    clickToSearch () {
      this.selectByPageByFilter()
    },
    async selectByPageByFilter () {
      const { results, totalRecord } = await listService.selectByPageByFilter(this.form)
      this.tableData = results
      this.totalRecord = totalRecord
    },
    clickToChangePage (page) {
      this.form.page = page
      this.selectByPageByFilter()
    },
    clickToChangeRows (rows) {
      this.form.rows = rows
      this.selectByPageByFilter()
    },
    getStatusClass (index) {
      let className = 'table-block-status-'
      switch (index) {
      case 0:
        className += 'primary'
        break
      case 1:
        className += 'danger'
        break
      case 2:
        className += 'info'
        break
      case 4:
        className += 'success'
        break
      default:
        className += 'primary'
      }

      return className
    }
  }
}
