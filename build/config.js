const title = '美特斯邦威后台'

const proxy = {
  '/permission.web': {
    // 目标 API 地址
    target: 'http://ump-permission.fi.com/',
    changeOrigin: true,
    onProxyRes: function (proxyRes, req, res) {
      const cookies = proxyRes.headers['set-cookie']
      const cookieRegex = /Path=\/permission.web\//i
      // 修改cookie Path
      if (cookies) {
        var newCookie = cookies.map(function (cookie) {
          if (cookieRegex.test(cookie)) {
            return cookie.replace(cookieRegex, 'Path=/')
          }
          return cookie
        })
        // 修改cookie path
        delete proxyRes.headers['set-cookie']
        proxyRes.headers['set-cookie'] = newCookie
      }
    }
  },
  '/framework.web': {
    target: 'http://ump-framework.fi.com',
    changeOrigin: true
  },
  '/pub-server': {
    target: 'http://10.100.33.168:8080',
    changeOrigin: true
  },
  '/crmMember': {
    target: 'http://crm-member.di.com',
    // target: 'http://10.9.5.199:8083',
    changeOrigin: true
  },
  '/crmEquity': {
    target: 'http://crm-equity.di.com',
    // target:'http://10.8.153.104:8083',
    changeOrigin: true
  },
  '/crmActivity': {
    target: 'http://crm-activity.di.com',
    changeOrigin: true
  },
  '/crmCoupon': {
    target: 'http://crm-coupon.di.com',
    // target:'http://10.8.153.104:8083',
    changeOrigin: true
  },
  '/crmPoint': {
    target: 'http://crm-point.di.com',
    // target:'http://10.8.153.104:8083',
    changeOrigin: true
  },
  '/crmWx': {
    target: 'http://crm-wx.di.com',
    changeOrigin: true
  }
}

module.exports = {
  title,
  proxy
}
